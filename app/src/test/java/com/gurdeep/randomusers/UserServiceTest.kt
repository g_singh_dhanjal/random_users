package com.gurdeep.randomusers

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.gurdeep.randomusers.network.RetrofitUserService
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UserServiceTest {
    lateinit var mockWebServer: MockWebServer


    @Mock
    lateinit var retrofitUserService: RetrofitUserService

    @Before
    fun onBefore() {

        mockWebServer = MockWebServer()
        mockWebServer.start()
        val context = ApplicationProvider.getApplicationContext<Context>()
        val httpUrl = mockWebServer.url(context.getString(R.string.base_url))
        //val result =   retrofitUserService.getUsers(2)

    }

    @Test
    fun testUserService() {
       // Mockito.`when`(retrofitUserService.getUsers(2)).thenReturn()
        //  val response =   result.execute()

    }


    @After
    fun teardown() {
        mockWebServer.shutdown()
    }
}