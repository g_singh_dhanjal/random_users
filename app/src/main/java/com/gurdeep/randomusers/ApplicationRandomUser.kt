package com.gurdeep.randomusers

import android.app.Application
import android.content.Context
import com.gurdeep.randomusers.di.AppComponent
import com.gurdeep.randomusers.di.DaggerAppComponent

class ApplicationRandomUser : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(
            this
        )
    }
    override fun onCreate() {
        super.onCreate()
    }


    fun getContextAppContext() = this

}