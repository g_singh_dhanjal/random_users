package com.gurdeep.randomusers.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gurdeep.randomusers.databinding.ListItemUserDetailBinding
import com.gurdeep.randomusers.viewmodel.data.User

class UsersAdapter(private val context: Context, private val listOfUsers: List<User>) :
    RecyclerView.Adapter<UsersViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val listItemBinding =
            ListItemUserDetailBinding.inflate(LayoutInflater.from(parent.context), null, false)
        println("data observed onCreateViewHolder ${listOfUsers.size} ")
        return UsersViewHolder(listItemBinding)
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        holder.bind(listOfUsers[position])
    }

    override fun getItemCount(): Int {
        return listOfUsers.size
    }
}


class UsersViewHolder(private val binding: ListItemUserDetailBinding) :
    RecyclerView.ViewHolder(binding.root) {


    fun bind(userList: User) {
        binding.user = userList
    }

}