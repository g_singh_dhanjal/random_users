package com.gurdeep.randomusers.viewmodel

import androidx.lifecycle.*
import com.gurdeep.randomusers.repository.LocalDataRepository
import com.gurdeep.randomusers.repository.RemoteRepository
import com.gurdeep.randomusers.result.Result
import com.gurdeep.randomusers.result.Result.Success
import com.gurdeep.randomusers.viewmodel.data.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserViewModel @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val localDataRepository: LocalDataRepository,

    ) : ViewModel() {


    var userLiveData =  localDataRepository.getAllUsers().asLiveData()

    fun fetchUsers() {
        viewModelScope.launch {
            val result = remoteRepository.fetchAllUsers()
            println("result in user-view-model ")
            println(result)
            when (result) {
                is Success -> showListItems(userList = result.userList)
                is Result.Error -> showErrorListItem(result.exception)

            }

        }
    }

    private suspend fun showListItems(userList: List<User>) {
         viewModelScope.launch {
             withContext(Dispatchers.IO
             ){
                 localDataRepository.insertAllUsers(userList)
             }
         }

    }

    private fun showErrorListItem(exception: String) {
        //userLiveData.value = emptyList()

    }

}