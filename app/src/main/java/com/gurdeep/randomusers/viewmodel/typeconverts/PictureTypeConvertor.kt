package com.gurdeep.randomusers.viewmodel.typeconverts

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.gurdeep.randomusers.viewmodel.data.Id
import com.gurdeep.randomusers.viewmodel.data.Picture



class PictureTypeConvertor : GsonConvertor(){

    @TypeConverter
    fun fromPictureToString(picture: Picture?)=gson.toJson(picture)
    @TypeConverter
    fun fromStringToPicture(picture: String?)= gson.fromJson(picture,Picture::class.java)

}