package com.gurdeep.randomusers.viewmodel.typeconverts

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.gurdeep.randomusers.viewmodel.data.Coordinates
import com.gurdeep.randomusers.viewmodel.data.Registered


class CoordinatesTypeConvertor: GsonConvertor() {

    @TypeConverter
    fun fromCoordinatesToString(coordinates: Coordinates?)=gson.toJson(coordinates)
    @TypeConverter
    fun fromStringToCoordinates(coordinates: String?)= gson.fromJson(coordinates,Coordinates::class.java)

}