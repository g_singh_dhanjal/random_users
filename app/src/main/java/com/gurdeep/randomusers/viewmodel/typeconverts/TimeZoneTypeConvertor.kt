package com.gurdeep.randomusers.viewmodel.typeconverts

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.gurdeep.randomusers.viewmodel.data.Registered
import com.gurdeep.randomusers.viewmodel.data.TimeZone


class TimeZoneTypeConvertor: GsonConvertor() {

    @TypeConverter
    fun fromTimeZoneToString(timeZone: TimeZone?)= gson.toJson(timeZone)
    @TypeConverter
    fun fromStringToTimeZone(timeZone: String?)= gson.fromJson(timeZone,TimeZone::class.java)


}