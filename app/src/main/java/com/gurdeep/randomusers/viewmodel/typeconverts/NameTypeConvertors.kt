package com.gurdeep.randomusers.viewmodel.typeconverts

import androidx.room.TypeConverter
import com.gurdeep.randomusers.viewmodel.data.Name
import javax.inject.Inject

class NameTypeConvertors() : GsonConvertor() {



    @TypeConverter
    fun fromNameToString(name: Name?): String {


        return gson.toJson(name)

    }

    @TypeConverter
    fun fromStringToName(name: String?) = gson.fromJson(name, Name::class.java)

}