package com.gurdeep.randomusers.viewmodel.typeconverts

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.gurdeep.randomusers.viewmodel.data.DOB
import com.gurdeep.randomusers.viewmodel.data.Id
import java.net.IDN



class IdTypeConvertor : GsonConvertor(){

    @TypeConverter
    fun fromIdToString(id: Id?)=gson.toJson(id)
    @TypeConverter
    fun fromStringToId(id: String?)= gson.fromJson(id,Id::class.java)

}