package com.gurdeep.randomusers.viewmodel.typeconverts

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.gurdeep.randomusers.viewmodel.data.DOB
import com.gurdeep.randomusers.viewmodel.data.Registered

class DobTypeConvertors : GsonConvertor(){

    @TypeConverter
    fun fromDobToString(dob: DOB?)=gson.toJson(dob)
    @TypeConverter
    fun fromStringToDob(dob: String?)= gson.fromJson(dob,DOB::class.java)

}