package com.gurdeep.randomusers.viewmodel.data

data class Coordinates(val longitude: Double=0.0, val latitude: Double=0.0) {

}