package com.gurdeep.randomusers.viewmodel.data

data class Picture(val thumbnail: String?="", val medium: String?="", val large:String?="") {
}