package com.gurdeep.randomusers.viewmodel.data

data class Location(val street: Street?=null, val city:String? ="", val state:String? ="", val country:String?="", val postCode: Int=0) {
}