package com.gurdeep.randomusers.viewmodel.data


data class Name(val title: String?="", val first: String?="", val last: String?="") {
}