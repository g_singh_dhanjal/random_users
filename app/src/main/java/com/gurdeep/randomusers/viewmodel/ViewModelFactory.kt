package com.gurdeep.randomusers.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gurdeep.randomusers.repository.LocalDataRepository
import com.gurdeep.randomusers.repository.RemoteRepository
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
class ViewModelFactory @Inject constructor(private val viewModels:MutableMap<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel> > ) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        viewModels[modelClass]?.get() as T


}