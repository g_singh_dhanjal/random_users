package com.gurdeep.randomusers.viewmodel.typeconverts

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.gurdeep.randomusers.viewmodel.data.Registered
import com.gurdeep.randomusers.viewmodel.data.Street



class StreetTypeConvertor : GsonConvertor(){

    @TypeConverter
    fun fromStreetToString(street: Street?)=gson.toJson(street)
    @TypeConverter
    fun fromStringToStreet(street: String?)= gson.fromJson(street,Street::class.java)

}