package com.gurdeep.randomusers.viewmodel.typeconverts

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.gurdeep.randomusers.viewmodel.data.Location
import com.gurdeep.randomusers.viewmodel.data.Login


class LoginTypeConvertor : GsonConvertor(){

    @TypeConverter
    fun fromLoginToString(login: Login?)=gson.toJson(login)
    @TypeConverter
    fun fromStringToLogin(login: String?)= gson.fromJson(login,Login::class.java)

}