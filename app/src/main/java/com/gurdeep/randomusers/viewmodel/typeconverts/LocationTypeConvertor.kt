package com.gurdeep.randomusers.viewmodel.typeconverts

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.gurdeep.randomusers.viewmodel.data.Location
import com.gurdeep.randomusers.viewmodel.data.Name




class LocationTypeConvertor : GsonConvertor(){

    @TypeConverter
    fun fromLocationToString(location: Location?)=gson.toJson(location)
    @TypeConverter
    fun fromStringToLocation(location: String?)= gson.fromJson(location,Location::class.java)
}