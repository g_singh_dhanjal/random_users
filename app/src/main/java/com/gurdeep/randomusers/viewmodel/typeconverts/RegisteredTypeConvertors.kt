package com.gurdeep.randomusers.viewmodel.typeconverts

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.gurdeep.randomusers.viewmodel.data.Login
import com.gurdeep.randomusers.viewmodel.data.Registered



class RegisteredTypeConvertors: GsonConvertor() {

    @TypeConverter
    fun fromRegisteredToString(registered: Registered?)=gson.toJson(registered)
    @TypeConverter
    fun fromStringToRegistered(registered: String?)= gson.fromJson(registered,Registered::class.java)

}