package com.gurdeep.randomusers.viewmodel.data

data class TimeZone(val offset: String?="", val description: String?="") {
}