package com.gurdeep.randomusers.viewmodel.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.gurdeep.randomusers.viewmodel.typeconverts.*


@Entity
data class  User(
    @PrimaryKey(autoGenerate = true)
    var userDbId: Long=1,

    var gender: String,
    @TypeConverters(NameTypeConvertors::class)
    var name: Name,
    @TypeConverters(LocationTypeConvertor::class)
    val location: Location?,
    val email: String?,
    @TypeConverters(LoginTypeConvertor::class)
    val login: Login?,
    @TypeConverters(DobTypeConvertors::class)
    val dob: DOB?,
    @TypeConverters(RegisteredTypeConvertors::class)
    val registered: Registered?,
    val phone: String?,
    val cell: String?,
    @TypeConverters(IdTypeConvertor::class)
    val id: Id?,
    @TypeConverters(PictureTypeConvertor::class)
    val picture: Picture?,
    val nat: String
) {
}