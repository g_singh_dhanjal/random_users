package com.gurdeep.randomusers.repository

import com.gurdeep.randomusers.network.RetrofitUserService
import com.gurdeep.randomusers.result.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class RemoteRepository @Inject constructor(
) {

    @Inject
    lateinit var userService: RetrofitUserService
    suspend fun  fetchAllUsers(): Result {

        return withContext(Dispatchers.IO){
            var result: Result = Result.Error("")
            val userList: Call<com.gurdeep.randomusers.viewmodel.data.Results> = userService.getUsers(1)
            val response =   userList.execute()
            if (response.isSuccessful){
                println(response.body())
                result = Result.Success((response.body() as com.gurdeep.randomusers.viewmodel.data.Results).results)
            }
            else {
                println("Error  from the service")

                result = Result.Error("Error from server ")
            }



            return@withContext result
        }

    }


}