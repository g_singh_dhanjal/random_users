package com.gurdeep.randomusers.repository

import com.gurdeep.randomusers.database.dao.UserDao
import com.gurdeep.randomusers.viewmodel.data.User
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

public class LocalDataRepository @Inject constructor(
    private val userDao: UserDao
) {

    suspend fun insertAllUsers(userList: List<User>) {
        println("Total insertion in the database ${userDao.insertAll(userList)}")
        //println("Total users in the database ${userDao.getAllUser()}")
    }

     fun getAllUsers() = userDao.getAllUser()

}