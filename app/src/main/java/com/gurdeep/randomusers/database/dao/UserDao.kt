package com.gurdeep.randomusers.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gurdeep.randomusers.viewmodel.data.User


@Dao
abstract  class  UserDao {
    @Query("Select * from User")
    abstract  fun getAllUser(): kotlinx.coroutines.flow.Flow<List<User>>
    @Query("Select * from User")
    abstract  fun getAllUsers():List<User>
    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    abstract fun insert( user: User) : Long

    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    abstract  fun insertAll( user: List<User>) : List<Long>


}