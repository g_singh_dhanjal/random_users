package com.gurdeep.randomusers.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.gurdeep.randomusers.database.dao.UserDao
import com.gurdeep.randomusers.viewmodel.data.User
import com.gurdeep.randomusers.viewmodel.typeconverts.*


@Database(entities = arrayOf(User::class), version = 1)
@TypeConverters(
    NameTypeConvertors::class,
    LocationTypeConvertor::class,
    CoordinatesTypeConvertor::class,
    DobTypeConvertors::class,
    IdTypeConvertor::class,
    LoginTypeConvertor::class,
    PictureTypeConvertor::class,
    RegisteredTypeConvertors::class,
    StreetTypeConvertor::class,
    TimeZoneTypeConvertor::class
)


abstract class RandomUserDb : RoomDatabase() {
    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var instance: RandomUserDb? = null
        private val LOCK = Any()

        /*fun getDatabaseInstance(applicationContext: Context): RandomUserDb {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(applicationContext).also { it }
            }
        }*/

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also{
                instance = it
            }
        }

        private fun buildDatabase(context: Context): RandomUserDb {
            return Room.databaseBuilder(context, RandomUserDb::class.java, "random_user").build()
        }

    }

}