package com.gurdeep.randomusers.di.subcomponent

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import com.gurdeep.randomusers.UserListActivity
import dagger.Component
import dagger.Provides
import dagger.Subcomponent

@Subcomponent
interface UserSubComponent {
@Subcomponent.Factory
interface  Factory {
   fun create(): UserSubComponent
}
   fun inject(userListActivity: UserListActivity)



}