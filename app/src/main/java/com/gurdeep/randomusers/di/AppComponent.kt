package com.gurdeep.randomusers.di

import android.content.Context
import com.google.gson.Gson
import com.gurdeep.randomusers.di.module.DatabaseModule
import com.gurdeep.randomusers.di.module.NetworkModule
import com.gurdeep.randomusers.di.module.ParsingModule
import com.gurdeep.randomusers.di.module.ViewModelModule
import com.gurdeep.randomusers.di.subcomponent.AppSubComponent
import com.gurdeep.randomusers.di.subcomponent.UserSubComponent
import com.gurdeep.randomusers.viewmodel.typeconverts.GsonConvertor
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        AppSubComponent::class,
        NetworkModule::class,
        ViewModelModule::class,
        DatabaseModule::class,
        ParsingModule::class
    )
)
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun createUserSubComponent(): UserSubComponent.Factory

}