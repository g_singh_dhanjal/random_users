package com.gurdeep.randomusers.di.module

import android.content.Context
import com.gurdeep.randomusers.database.RandomUserDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Singleton
    @Provides
    fun providesRoomInstance(context: Context) = RandomUserDb.invoke(context)

    @Singleton
    @Provides
    fun providesUserDao(randomUserDb: RandomUserDb) = randomUserDb.userDao()
}