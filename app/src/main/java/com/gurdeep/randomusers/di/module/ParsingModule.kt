package com.gurdeep.randomusers.di.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ParsingModule {

    @Provides
    @Singleton
    fun providesGsonInstance() = Gson()

}