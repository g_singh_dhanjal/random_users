package com.gurdeep.randomusers.di.subcomponent

import dagger.Module

@Module(subcomponents = arrayOf(UserSubComponent::class))
class AppSubComponent {
}