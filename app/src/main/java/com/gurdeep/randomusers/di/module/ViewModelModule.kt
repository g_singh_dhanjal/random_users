package com.gurdeep.randomusers.di.module

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.gurdeep.randomusers.di.ViewModelKey
import com.gurdeep.randomusers.viewmodel.UserViewModel
import com.gurdeep.randomusers.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class ViewModelModule  {

    @Binds
    abstract fun bindViewModelFactory( viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel::class)
    abstract  fun userViewModel(userViewModel: UserViewModel):ViewModel

}