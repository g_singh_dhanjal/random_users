package com.gurdeep.randomusers

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation

class AppBindingAdapters {

    companion object {
        @BindingAdapter(value = ["imageUrl", "placeholder"], requireAll = false)
        @JvmStatic
        fun setImageUrl(imageView: ImageView, url: String?, placeHolder: Drawable?) {
            if (url == null) {
                imageView.setImageDrawable(placeHolder);
            } else {
                val transformation: Transformation = RoundedTransformationBuilder()
                    .borderColor(Color.BLACK)
                    .borderWidthDp(0f)
                    .cornerRadiusDp(30f)
                    .oval(false)
                    .build()
                Picasso.get().load(url).transform(transformation) .into(imageView)
            }
        }
    }
}