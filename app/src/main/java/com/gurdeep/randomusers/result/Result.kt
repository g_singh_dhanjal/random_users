package com.gurdeep.randomusers.result

import com.gurdeep.randomusers.viewmodel.data.User

sealed class Result {

    data class Success(val userList: List<User>) : Result() {}
    data class Error(val exception: String) : Result() {}

}