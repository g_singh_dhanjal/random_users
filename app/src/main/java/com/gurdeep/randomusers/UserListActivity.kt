package com.gurdeep.randomusers

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.gurdeep.randomusers.adapter.UsersAdapter
import com.gurdeep.randomusers.databinding.ActivityMainBinding
import com.gurdeep.randomusers.viewmodel.UserViewModel
import com.gurdeep.randomusers.viewmodel.ViewModelFactory
import javax.inject.Inject

class UserListActivity : AppCompatActivity() {


    // lateinit var usersAdapter: UsersAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val userViewModel by viewModels<UserViewModel>() {
        viewModelFactory
    }


    @Inject
    lateinit var linearLayoutManager: LinearLayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        val activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)
        activityMainBinding.recyclerView.layoutManager =
            LinearLayoutManager(this@UserListActivity, LinearLayoutManager.VERTICAL, false)
        activityMainBinding.recyclerView.itemAnimator = DefaultItemAnimator()
        activityMainBinding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )
        (applicationContext as ApplicationRandomUser).appComponent.createUserSubComponent().create()
            .inject(this)


        userViewModel.userLiveData.observe(this, Observer {

            it ?: return@Observer
            println("data observed= ${it.size} ")
            val usersAdapter = UsersAdapter(this@UserListActivity, it)
            activityMainBinding.recyclerView.adapter = usersAdapter
            usersAdapter.notifyDataSetChanged()
        })



        activityMainBinding.button.setOnClickListener {

            fetchUsers()
        }
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        fetchUsers()
        super.onResume()
    }

    fun fetchUsers() {
        userViewModel.fetchUsers()
    }
}