package com.gurdeep.randomusers.network

import com.gurdeep.randomusers.viewmodel.data.Results
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitUserService {

    @GET("api/")
      fun getUsers(@Query("results")results:Int): Call<Results>
}