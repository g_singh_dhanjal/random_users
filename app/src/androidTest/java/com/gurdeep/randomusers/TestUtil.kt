package com.gurdeep.randomusers

import com.gurdeep.randomusers.viewmodel.data.*

class TestUtil {

    companion object {
        fun createUser(id : Long, name: String): User {

            return User(
                userDbId = id, "Male",
                name = Name(title = "Mr.",first =name,last = "Singh").apply { },
                location = Location(street = Street(name = "Kolmar Road",number = "25"),city = "Auckand",state = "Auckland",country = "NZ",postCode = 6025).apply {},
                email = "gurdeep.singh180@gmail.com",
                login = Login(uuid = "fsdjghkj",username = "gurdeep.singh",password = "*******",salt = ""),
                dob = DOB(date = "20-01-1991",age = 30),
                registered = Registered(date = "20-01-2021",age = 30),
                phone = "+6477895252",
                cell = "045678955",
                Id(name = "",value = "54564564"),
                picture = Picture(thumbnail = "",medium = "",large = ""),
                nat = "NZ"


            )
        }
    }

}