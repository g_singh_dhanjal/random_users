package com.gurdeep.randomusers

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.gurdeep.randomusers.database.RandomUserDb
import com.gurdeep.randomusers.database.dao.UserDao
import com.gurdeep.randomusers.viewmodel.data.User
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.hamcrest.collection.IsCollectionWithSize.hasSize
import org.hamcrest.core.Every.everyItem

import org.junit.Assert.*

@RunWith(AndroidJUnit4::class)
class UserEntityDatabaseTest {

    lateinit var db: RandomUserDb
    lateinit var userDao: UserDao

    @Before
    fun createDb() {

        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.databaseBuilder(context, RandomUserDb::class.java, "random_user").build()
        userDao = db.userDao()
    }

    @Test
    fun testSingleInsertionInUserTable() {
        val dbId = userDao.insert(TestUtil.createUser(2, "Gurdeep"))
        assertEquals(2, dbId)

        val allUsers :List<User> = userDao.getAllUsers()
        assertThat(allUsers.size, greaterThan(0))
    }

    @Test
    fun testCollectionInsertionInUserTable() {

        val allIds = userDao.insertAll(emptyList<User>())
        assertEquals(0, allIds.size)
    }




    @After
    fun tearDown() {
        db.close()

    }


}