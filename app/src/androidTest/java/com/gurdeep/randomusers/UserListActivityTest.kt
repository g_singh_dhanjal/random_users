package com.gurdeep.randomusers

import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class UserListActivityTest {

    @get:Rule
    var activityScenarioRule = ActivityScenarioRule<UserListActivity>(UserListActivity::class.java)


    @Test
    fun testUserListActivity() {
        val scenario = activityScenarioRule.scenario
        scenario.moveToState(Lifecycle.State.RESUMED)


    }

    @Test
    fun testClickEvent() {
        Espresso.onView(ViewMatchers.withId(R.id.button)).perform(ViewActions.click())
    }
}